class ArticlesController < ApplicationController
  def index
    @articles = Storage.order(created_at: :desc).paginate(:page => params[:page], :per_page => 30)
  end
end
