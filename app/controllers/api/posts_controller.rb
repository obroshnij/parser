class Api::PostsController < ApplicationController
    skip_before_filter :verify_authenticity_token

    def index
      @posts = Storage.all
      respond_to do |format|
        format.xml { render xml: @posts, except: [:id, :created_at, :updated_at] }
        format.json { render json: @posts, except: [:id, :created_at, :updated_at] }
      end
    end

    def show
      @post = Storage.find(post_params[:id])
      respond_to do |format|
        format.xml { render xml: @post, except: [:id, :created_at, :updated_at] }
        format.json { render json: @post, except: [:id, :created_at, :updated_at] }
      end
    end

    private
    def post_params
      params.permit(:id)
    end

end