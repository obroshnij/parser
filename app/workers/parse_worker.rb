require 'nokogiri'
require 'open-uri'

class ParseWorker
  include Sidekiq::Worker

  def perform()
    (1..5).each do |page_num|
      puts "============Page: #{page_num}==========="    
      url = "https://news.ycombinator.com/news?p=#{page_num}"
      current_page = Nokogiri::HTML(open(url))
      item_count = current_page.css('tr.athing').count
      (0..(item_count-1)).try(:each) do |it|
        url = current_page.css('tr.athing td.title a.storylink')[it]["href"]
        title = current_page.css('tr.athing td.title a.storylink')[it].text
        author = current_page.css('tr').css('td.subtext')[it].css('a')[0].text
        print 'url ', url, 'author ', author, 'title', title, "\n"
        Storage.where(url: url).first_or_create(title: title.html_safe, author: author.html_safe)
      end
    end
  end

end

ParseWorker.perform_async()
