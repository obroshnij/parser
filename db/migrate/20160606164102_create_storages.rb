class CreateStorages < ActiveRecord::Migration
  def change
    create_table :storages do |t|
      t.string :title
      t.string :author
      t.string :url
      t.timestamps null: false
    end
  end
end
