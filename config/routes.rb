Rails.application.routes.draw do

  root 'articles#index'

  namespace :api do
    resources :posts, :defaults => { :format => 'json' }
  end
 
end
