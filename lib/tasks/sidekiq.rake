
namespace :sidekiq do

  desc "Start sidekiq"
  task :start do
    puts "Starting Sidekiq..."
    system "bundle exec sidekiq -e#{Rails.env} -d -L log/sidekiq.log"
    puts "Sidekiq started!"
  end

end
